package com.example.pixured2.peliculas;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by pixured2 on 10/2/15.
 */
public class DetailActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new DetailFragment())
                    .commit();
        }
    }

    public static class DetailFragment extends Fragment {

        private static final String LOG_TAG = DetailFragment.class.getSimpleName();
        public View rootView;
        Button youtubeButton;
        private static final String MOVIE_SHARE_HASHTAG= "#DalePaLasMovies!";
        public int mMovieID;
        public static LinearLayout mLayout;
        public ArrayList<String> list = new ArrayList<String>();
        public static ArrayAdapter<String> itemsAdapter;


        public DetailFragment() {
            setHasOptionsMenu(true);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_detail, container, false);

            mLayout = (LinearLayout) rootView.findViewById(R.id.listview_container);
            itemsAdapter = new ArrayAdapter<String>(getContext(), R.layout.comment_item, list);
            youtubeButton = (Button) rootView.findViewById(R.id.detail_youtube_button);

            // The detail Activity called via intent.  Inspect the intent for forecast data.
            Intent intent = getActivity().getIntent();
            if (intent != null && intent.hasExtra(Intent.EXTRA_TEXT)) {
                //Get the movie ID
                mMovieID = Integer.parseInt(intent.getStringExtra(Intent.EXTRA_TEXT));

                //BOTH RUNTIME AND TRAILER are set here! Only call once for those things...
                if (MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.RUNTIME_KEY)] == null) {
                    FetchMovieTask needRuntime = new FetchMovieTask();
                    needRuntime.execute(MainActivityFragment.fakeDb[mMovieID][getResources()
                            .getInteger(R.integer.MOVIE_TMDB_ID)], mMovieID, getActivity());
                } else{
                    ((TextView) rootView.findViewById(R.id.movie_runtime))
                            .setText(MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.RUNTIME_KEY)]);
                    youtubeButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            // Perform action on click
                            if (MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.TRAILER_KEY)] != null) {
                                Intent viewClip = new Intent(Intent.ACTION_VIEW, Uri.parse(MainActivityFragment
                                        .fakeDb[mMovieID][getResources().getInteger(R.integer.TRAILER_KEY)]));
                                startActivity(viewClip);
                            } else {
                                Toast.makeText(rootView.getContext(), "There is no trailer :(", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

                new FetchCommentsTask().execute(MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.MOVIE_TMDB_ID)], list);

                //Set Movie Title and Background color
                TextView ourView = ((TextView) rootView.findViewById(R.id.detail_text));
                ourView.setText(MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.TITLE_KEY)]);
                ourView.setBackgroundColor(Color.rgb(51, 153, 102));
                ourView.setTextColor(Color.WHITE);

                //Set Image Poster
                Picasso.with(getActivity())
                        .load(MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.POSTER_PATH_KEY)])
                        .into((ImageView) rootView.findViewById(R.id.movie_thumb));

                //Set Description
                ((TextView) rootView.findViewById(R.id.overview_textview))
                        .setText(MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.OVERVIEW_KEY)]);

                //Set Year
                ((TextView) rootView.findViewById(R.id.movie_year))
                        .setText(MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.YEAR_KEY)]);
                //((TextView) rootView.findViewById(R.id.movie_year)).setTextColor(Color.BLACK);

                //Set Rating
                ((TextView) rootView.findViewById(R.id.movie_rating))
                        .setText((MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.RATING_KEY)]) + "/10");
                ((TextView) rootView.findViewById(R.id.movie_rating)).setTextColor(Color.BLACK);

                /*//Set Popularity
                ((TextView) rootView.findViewById(R.id.movie_popularity))
                        .setText("Pop:" + (MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.POPULARITY_KEY)]));*/

                /*//Set Page
                ((TextView) rootView.findViewById(R.id.movie_page))
                        .setText("Page:" + (MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.PAGE_KEY)]));*/

                /*mListview = (LinearLayout) rootView.findViewById(R.id.listview_container);
                mListview.setAdapter(itemsAdapter);*/
            }

            return rootView;
        }

        public class FetchMovieTask extends AsyncTask<Object, Void, String> {

            @Override
            protected String doInBackground(Object... params) {

                final String TMDB_API_KEY = "f5850f018bc96b34140552d40cf15669";
                final String OWM_RUNTIME = "runtime";
                final String OWM_TRAILER = "trailers";
                final String OWM_YOUTUBE = "youtube";
                final String OWM_SOURCE = "source";

                // These two need to be declared outside the try/catch
                // so that they can be closed in the finally block.
                HttpURLConnection urlConnection = null;
                BufferedReader reader = null;

                // Will contain the raw JSON response as a string.
                String movieJsonStr = "";
                try {
                    // Construct the URL for the OpenWeatherMap query
                    // Possible parameters are avaiable at OWM's forecast API page, at
                    // http://openweathermap.org/API#forecast
                    final String MOVIE_BASE_URL =
                            "http://api.themoviedb.org/3/movie/" + params[0] + "?";
                    final String API_KEY = "api_key";
                    final String APPEND_KEY = "append_to_response";

                    Uri builtUri = Uri.parse(MOVIE_BASE_URL).buildUpon()
                            .appendQueryParameter(API_KEY, TMDB_API_KEY)
                            .appendQueryParameter(APPEND_KEY, "trailers")
                            .build();

                    URL url = new URL(builtUri.toString());

                    //Log.d(LOG_TAG, "Attempting to pull movies from: " + builtUri.toString());

                    // Create the request to OpenWeatherMap, and open the connection
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.connect();

                    // Read the input stream into a String
                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuffer buffer = new StringBuffer();
                    if (inputStream == null) {
                        // Nothing to do.
                        return null;
                    }
                    reader = new BufferedReader(new InputStreamReader(inputStream));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                        // But it does make debugging a *lot* easier if you print out the completed
                        // buffer for debugging.
                        buffer.append(line + "\n");
                    }

                    if (buffer.length() == 0) {
                        // Stream was empty.  No point in parsing.
                        return null;
                    }
                    movieJsonStr = buffer.toString();

                } catch (IOException e) {
                    Log.e(LOG_TAG, "Error ", e);
                    // If the code didn't successfully get the weather data, there's no point in attemping
                    // to parse it.
                    return null;
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (final IOException e) {
                            Log.e(LOG_TAG, "Error closing stream", e);
                        }
                    }
                }

                try {
                    //format = https://www.youtube.com/watch?v=OIx8OGeEh9c

                    JSONObject movieJSON = new JSONObject(movieJsonStr);
                    String runtime = movieJSON.getString(OWM_RUNTIME);

                    JSONObject trailers = movieJSON.getJSONObject(OWM_TRAILER);
                    JSONArray youtubeTrailers = trailers.getJSONArray(OWM_YOUTUBE);
                    if(!youtubeTrailers.isNull(0)){
                        String finalTrailer = "https://www.youtube.com/watch?v=" +
                                youtubeTrailers.getJSONObject(0).getString(OWM_SOURCE);
                        MainActivityFragment.fakeDb[(int) params[1]][getContext().getResources().getInteger(R.integer.TRAILER_KEY)] = finalTrailer;
                    }

                    MainActivityFragment.fakeDb[(int) params[1]][getContext().getResources().getInteger(R.integer.RUNTIME_KEY)] = runtime + "min";

                    return runtime + "min";
                } catch (JSONException e) {
                    Log.e(LOG_TAG, e.getMessage(), e);
                    e.printStackTrace();
                }
                return "Internal Error";
            }

            @Override
            public void onPostExecute(String result) {
                ((TextView) rootView.findViewById(R.id.movie_runtime))
                        .setText(MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.RUNTIME_KEY)]);

                youtubeButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // Perform action on click
                        if(MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.TRAILER_KEY)] != null) {
                            Intent viewClip = new Intent(Intent.ACTION_VIEW, Uri.parse(MainActivityFragment
                                    .fakeDb[mMovieID][getResources().getInteger(R.integer.TRAILER_KEY)]));
                            startActivity(viewClip);
                        } else {
                            Toast.makeText(rootView.getContext(), "There is no trailer :(", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            // Inflate the menu; this adds items to the action bar if it is present.
            inflater.inflate(R.menu.detailfragment, menu);

            // Retrieve the share menu item
            MenuItem menuItem = menu.findItem(R.id.action_share);

            // Get the provider and hold onto it to set/change the share intent.
            ShareActionProvider mShareActionProvider =
                    (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);

            // Attach an intent to this ShareActionProvider.  You can update this at any time,
            // like when the user selects a new piece of data they might like to share.
            if (mShareActionProvider != null ) {
                mShareActionProvider.setShareIntent(createShareForecastIntent());
            } else {
                Log.d(LOG_TAG, "Share Action Provider is null?");
            }
        }

        private Intent createShareForecastIntent() {
            String title = MainActivityFragment.fakeDb[mMovieID][getResources().getInteger(R.integer.TITLE_KEY)];
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT,
                    "OMG! I love \"" + title + "\"" + MOVIE_SHARE_HASHTAG);
            return shareIntent;
        }
    }

}
