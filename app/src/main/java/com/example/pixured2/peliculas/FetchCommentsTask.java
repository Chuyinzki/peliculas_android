package com.example.pixured2.peliculas;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by pixured2 on 10/6/15.
 */
public class FetchCommentsTask extends AsyncTask<Object, Void, String> {
    final String LOG_TAG = FetchCommentsTask.class.getSimpleName();

    protected String doInBackground(Object... params) {

        final String TMDB_API_KEY = "f5850f018bc96b34140552d40cf15669";
        final String OWM_RESULTS = "results";
        final String OWM_AUTHOR = "author";
        final String OWM_CONTENT = "content";

        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String reviewJsonStr = "";
        try {
            // Construct the URL for the OpenWeatherMap query
            // Possible parameters are avaiable at OWM's forecast API page, at
            // http://openweathermap.org/API#forecast
            final String MOVIE_BASE_URL =
                    "http://api.themoviedb.org/3/movie/" + params[0] + "/reviews?";
            final String API_KEY = "api_key";

            Uri builtUri = Uri.parse(MOVIE_BASE_URL).buildUpon()
                    .appendQueryParameter(API_KEY, TMDB_API_KEY)
                    .build();

            Log.d(LOG_TAG, "Pulling comments from: " + builtUri.toString());

            URL url = new URL(builtUri.toString());

            //Log.d(LOG_TAG, "Attempting to pull movies from: " + builtUri.toString());

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            reviewJsonStr = buffer.toString();

        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attemping
            // to parse it.
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }

        try {
            JSONObject movieJSON = new JSONObject(reviewJsonStr);
            JSONArray commentsArray = movieJSON.getJSONArray(OWM_RESULTS);

            //JSONObject trailers = movieJSON.getJSONObject(OWM_TRAILER);
            //JSONArray youtubeTrailers = trailers.getJSONArray(OWM_YOUTUBE);
            for(int i = 0; i < commentsArray.length(); i++){
                JSONObject comment = commentsArray.getJSONObject(i);
                String finalComment = comment.getString(OWM_AUTHOR) + ": " + comment.getString(OWM_CONTENT);
                ((ArrayList<String>) params[1]).add(finalComment);
            }

            return "Done";
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return "Internal Error";
    }

    @Override
    public void onPostExecute(String result) {
        //DetailActivity.DetailFragment.itemsAdapter.notifyDataSetChanged();
        Log.d(LOG_TAG, "Size of our list = " + DetailActivity.DetailFragment.itemsAdapter.getCount());
        for(int i = 0; i < DetailActivity.DetailFragment.itemsAdapter.getCount(); i++){
            View item = DetailActivity.DetailFragment.itemsAdapter.getView(i, null, null);
            DetailActivity.DetailFragment.mLayout.addView(item);
        }
    }

}
