package com.example.pixured2.peliculas;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {
    private final String LOG_TAG = MainActivityFragment.class.getSimpleName();
    public static String[][] fakeDb = {};
    public static ImageAdapter mMoviesAdapter;
    // These are the names of the JSON objects that need to be extracted.
    public final String OWM_RESULTS = "results";
    public final String OWM_POSTERPATH = "poster_path";
    public final String OWM_TITLE = "title";
    public final String OWM_YEAR = "release_date";
    public final String OWM_MOVIE_ID = "id";
    public final String OWM_RATING = "vote_average";
    public final String OWM_OVERVIEW = "overview";
    public final String OWM_POPULARITY = "popularity";
    public final String OWM_RUNTIME = "runtime";
    public static int pageToLoad = 1;
    public static boolean layoutChanged = true;
    //initialize it in your activity so that the handler is bound to UI thread
    Handler handlerUI = new Handler();
    private boolean canLoad = true;
    GridView mGridView;



    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Add this line in order for this fragment to handle menu events.
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.mainactivityfragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //int id = item.getItemId();
        /*if (id == R.id.action_refresh) {
            updateMovies();
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // The ArrayAdapter will take data from a source and
        // use it to populate the ListView it's attached to.
        mMoviesAdapter =
                new ImageAdapter(
                        getActivity(), // The current context (this activity)
                        fakeDb);
        if (pageToLoad == 1) updateMovies();

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mGridView = (GridView) rootView.findViewById(R.id.gridview_movie);
        mGridView.setAdapter(mMoviesAdapter);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                //String forecast = (String) mMoviesAdapter.getItemName(position);
                Intent intent = new Intent(getActivity(), DetailActivity.class)
                        .putExtra(Intent.EXTRA_TEXT, "" + position);
                startActivity(intent);
            }
        });

        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount >= totalItemCount && canLoad) {
                    canLoad = false;
                    Log.d(LOG_TAG, "We scrolled to the end!! Loading more movies...");
                    updateMovies();
                }
                //Here i want to wait 1 second then:

                handlerUI.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        canLoad = true;
                    }
                }, 3000);
            }
        });


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (pageToLoad == 1) updateMovies();
    }

    private void updateMovies() {
        FetchMoviesTask moviesTask = new FetchMoviesTask();
        SharedPreferences sharedPrefs =
                PreferenceManager.getDefaultSharedPreferences(getActivity());

        String sortOrder = sharedPrefs.getString(
                getString(R.string.pref_sort_key),
                getString(R.string.pref_sort_popular_value));

        //String sortOrder = getString(R.string.popularity_desc);

        moviesTask.execute(sortOrder, Integer.toString(pageToLoad));
        pageToLoad++;
    }

/*    @Override
    public void onStart {
        super.onStart();
        updateMovies();
    }*/

    public class FetchMoviesTask extends AsyncTask<String, Void, String[][]> {

        private final String LOG_TAG = FetchMoviesTask.class.getSimpleName();
        private final String TMDB_API_KEY = "f5850f018bc96b34140552d40cf15669";
        private final String BASE_POSTER_URL = "http://image.tmdb.org/t/p/";
        private final String PIC_SIZE_URL = "w185";

        /**
         * Take the String representing the complete forecast in JSON Format and
         * pull out the data we need to construct the Strings needed for the wireframes.
         * <p/>
         * Fortunately parsing is easy:  constructor takes the JSON string and converts it
         * into an Object hierarchy for us.
         */

        private String parseYear(String messyString) {
            //Log.d(LOG_TAG, "BUILDING THE YEAR STRING NOW!");
            String finalString = "";

            char[] arr = messyString.toCharArray();
            //Log.d(LOG_TAG, "arr[" + i + "] is " + Integer.parseInt(Character.toString(arr[i])));
            for (char c : arr) {
                try {
                    Integer.parseInt("" + c);
                } catch (NumberFormatException e) {
                    break;
                }
                finalString += Integer.parseInt("" + c);
            }
            return finalString;
        }

        private String getRuntime(String movieID){
            // These two need to be declared outside the try/catch
            // so that they can be closed in the finally block.
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            // Will contain the raw JSON response as a string.
            String movieJsonStr = "";
            try {
                // Construct the URL for the OpenWeatherMap query
                // Possible parameters are avaiable at OWM's forecast API page, at
                // http://openweathermap.org/API#forecast
                final String MOVIE_BASE_URL =
                        "http://api.themoviedb.org/3/movie/" + movieID + "?";
                final String API_KEY = "api_key";

                Uri builtUri = Uri.parse(MOVIE_BASE_URL).buildUpon()
                        .appendQueryParameter(API_KEY, TMDB_API_KEY)
                        .build();

                URL url = new URL(builtUri.toString());

                //Log.d(LOG_TAG, "Attempting to pull movies from: " + builtUri.toString());

                // Create the request to OpenWeatherMap, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return null;
                }
                movieJsonStr = buffer.toString();

            } catch (IOException e) {
                Log.e(LOG_TAG, "Error ", e);
                // If the code didn't successfully get the weather data, there's no point in attemping
                // to parse it.
                return null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }

            try {
                JSONObject movieJSON = new JSONObject(movieJsonStr);
                String runtime = movieJSON.getString(OWM_RUNTIME);
                //MainActivityFragment.fakeDb[movieID][getContext().getResources().getInteger(R.integer.RUNTIME_KEY)] = runtime + "min";
                return runtime + "min";
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage(), e);
                e.printStackTrace();
            }
            return "Internal Error";
        }

        private String[][] getMovieDataFromJson(String moviesJSONString, String page)
                throws JSONException {

            JSONObject moviesJson = new JSONObject(moviesJSONString);
            JSONArray moviesArray = moviesJson.getJSONArray(OWM_RESULTS);

            //Log.d(LOG_TAG, "FAKE DB SIZE IS: " + getContext().getResources().getInteger(R.integer.FAKE_DB_SIZE));
            String[][] resultStrs = new String[moviesArray.length()][getContext().getResources().getInteger(R.integer.FAKE_DB_SIZE)];

            for (int i = 0; i < moviesArray.length(); i++) {
                String posterPath;
                String title;
                String year;
                String rating;
                String overview;
                String popularity;
                String movieID;

                JSONObject movie = moviesArray.getJSONObject(i);

                // description is in a child array called "weather", which is 1 element long.
                //JSONObject posterPathObject = movie.getJSONArray(OWM_WEATHER).getJSONObject(0);
                posterPath = movie.getString(OWM_POSTERPATH);
                title = movie.getString(OWM_TITLE);
                rating = movie.getString(OWM_RATING);
                overview = movie.getString(OWM_OVERVIEW);
                year = parseYear(movie.getString(OWM_YEAR));
                popularity = movie.getString(OWM_POPULARITY);
                movieID = movie.getString(OWM_MOVIE_ID);

                //Log.d(LOG_TAG, "The rating is: " + rating);

                //JSONObject temperatureObject = dayForecast.getJSONObject(OWM_TEMPERATURE);
                //double high = temperatureObject.getDouble(OWM_MAX);
                //double low = temperatureObject.getDouble(OWM_MIN);

                String finalPosterURL = BASE_POSTER_URL + PIC_SIZE_URL + posterPath;
                //Log.d(LOG_TAG, "Adding poster string to array: " + finalPosterURL);
                resultStrs[i][getContext().getResources().getInteger(R.integer.POSTER_PATH_KEY)] = finalPosterURL;
                resultStrs[i][getContext().getResources().getInteger(R.integer.TITLE_KEY)] = title;
                resultStrs[i][getContext().getResources().getInteger(R.integer.RATING_KEY)] = rating;
                resultStrs[i][getContext().getResources().getInteger(R.integer.OVERVIEW_KEY)] = overview;
                resultStrs[i][getContext().getResources().getInteger(R.integer.YEAR_KEY)] = year;
                resultStrs[i][getContext().getResources().getInteger(R.integer.POPULARITY_KEY)] = popularity;
                resultStrs[i][getContext().getResources().getInteger(R.integer.PAGE_KEY)] = page;
                resultStrs[i][getContext().getResources().getInteger(R.integer.MOVIE_TMDB_ID)] = movieID;

                //This is a very special line. If uncommented it will create a connection for each movie
                //at startup to get the runtime strings ready. IT IS VERY SLOW!!! User will cry, don't uncomment.
                //But the implementation is there and will load the strings. If we leave commented out
                //the program will fetch the runtime of movie every time a movie detail is clicked instead.

                //resultStrs[i][getContext().getResources().getInteger(R.integer.RUNTIME_KEY)] = getRuntime(movieID);
            }
            return resultStrs;

        }

        @Override
        protected String[][] doInBackground(String... params) {

            // If there's no zip code, there's nothing to look up.  Verify size of params.
            if (params.length == 0) {
                return null;
            }

            // These two need to be declared outside the try/catch
            // so that they can be closed in the finally block.
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            // Will contain the raw JSON response as a string.
            String moviesJsonStr = "";

            //String format = "json";
            //we will be passing in 'popularity.desc' as parameter
            //String sortBy = params[0];


            try {
                // Construct the URL for the OpenWeatherMap query
                // Possible parameters are avaiable at OWM's forecast API page, at
                // http://openweathermap.org/API#forecast
                final String FORECAST_BASE_URL =
                        "http://api.themoviedb.org/3/discover/movie?";
                final String QUERY_PARAM = "sort_by";
                final String API_KEY = "api_key";
                final String PAGE_KEY = "page";
                final String VOTE_COUNT_KEY = "vote_count.gte";
                final int minimumVotes = getContext().getResources().getInteger(R.integer.MIN_REVIEWS);

                Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                        .appendQueryParameter(QUERY_PARAM, params[0])
                        .appendQueryParameter(PAGE_KEY, params[1])
                        .appendQueryParameter(VOTE_COUNT_KEY, Integer.toString(minimumVotes))
                        .appendQueryParameter(API_KEY, TMDB_API_KEY)
                        .build();

                URL url = new URL(builtUri.toString());

                Log.d(LOG_TAG, "Attempting to pull movies from: " + builtUri.toString());

                // Create the request to OpenWeatherMap, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return null;
                }
                moviesJsonStr = buffer.toString();

            } catch (IOException e) {
                Log.e(LOG_TAG, "Error ", e);
                // If the code didn't successfully get the weather data, there's no point in attemping
                // to parse it.
                return null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }

            try {
                return getMovieDataFromJson(moviesJsonStr, params[1]);
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage(), e);
                e.printStackTrace();
            }

            // This will only happen if there was an error getting or parsing the forecast.
            return null;
        }

        @Override
        protected void onPostExecute(String[][] result) {
            if (result != null) {
                fakeDb = mMoviesAdapter.addStrings(result);
                //mMoviesAdapter.setStrings(result);
                if (layoutChanged){
                    mGridView.smoothScrollToPosition(0);
                    layoutChanged = false;
                }
                mMoviesAdapter.notifyDataSetChanged();
                /*for(String dayForecastStr : result) {
                    mMoviesAdapter.add(dayForecastStr);
                }*/
                // New data is back from the server.  Hooray!
            }
        }
    }
}
