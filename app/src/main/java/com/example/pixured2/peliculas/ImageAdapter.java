package com.example.pixured2.peliculas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by pixured2 on 10/2/15.
 */
public class ImageAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private final String LOG_TAG = ImageAdapter.class.getSimpleName();

    private String[][] imageUrls;

    public ImageAdapter(Context context, String[][] imageUrls) {

        this.context = context;
        this.imageUrls = imageUrls;

        inflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return imageUrls.length;
    }

    public String getItem(int position) {
        return imageUrls[position][context.getResources().getInteger(R.integer.POSTER_PATH_KEY)];
    }

    public String getItemName(int position) {
        return imageUrls[position][context.getResources().getInteger(R.integer.TITLE_KEY)];
    }

    public long getItemId(int position) {
        return 0;
    }

    public void clear(){imageUrls = null;}

    public void setStrings(String[][] newUrls){
        imageUrls = newUrls;
    }

    public String[][] addStrings(String[][] newUrls) {
        if (imageUrls == null) imageUrls = newUrls;
        else imageUrls = append(imageUrls, newUrls);

        return imageUrls;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.image_item_movie, parent, false);
        }

        //Log.d(LOG_TAG, "Attempting to add imageURL at position: " + position);

        Picasso
                .with(context)
                .load(imageUrls[position][context.getResources().getInteger(R.integer.POSTER_PATH_KEY)])
                .fit() // will explain later
                .into((ImageView) convertView);

        return convertView;
    }

    private String[][] append(String[][] a, String[][] b) {
        String[][] result = new String[a.length + b.length][];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

}